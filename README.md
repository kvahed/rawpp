# README #

raw++ is a MATLAB reader for Syngo MR VB/VD raw files, which was written in C++ for better file IO performance. The current version is 0.1. 

### How do I get set up? ###

* Compile raw++ with 'mex -O rawpp.cpp'

### Usage ###

* [data, sync] = grawpp; 
* Choose file in GUI