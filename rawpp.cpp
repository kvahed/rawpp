#include "rawpp.hpp"

#define DLLEXPORT extern "C" __declspec(dllexport)
#pragma comment( linker, "/export:read" )
void read (const std::string& fname, int nlhs, mxArray *plhs[]) {
    VXFile vxf (fname, nlhs, plhs);
    if (!vxf.Status()) {
        vxf.Digest();
        vxf.Close();
    }
}

#define DLLEXPORT extern "C" __declspec(dllexport)
#pragma comment( linker, "/export:read" )
void read (const std::string& fname, Matrix<raw>* buf) {
    VXFile vxf (fname, buf);
    if (!vxf.Status()) {
        vxf.Digest();
        vxf.Close();
    }
}

#ifdef HAVE_MAT_H
#define DLLEXPORT extern "C" __declspec(dllexport)
#pragma comment( linker, "/export:mexFunction" )
void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    
    if (!nrhs==1)
        mexErrMsgTxt ("usage: [a,b] = rawpp ('<filename>','sync','rtfeedback');");
    
    std::string fname (mxArrayToString(prhs[0]));
    if (!fname.size())
        mexErrMsgTxt ("filename not specified!");
    
    read (fname, nlhs, plhs);
    
}
#endif

