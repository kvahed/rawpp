/*
 *  This file is part of rawpp.
 *
 *  rawpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rawpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rawpp. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 30 Jul, 2015
 *      Author: Kaveh Vahedipour
 */

#ifndef __RAWPP_HPP__
#define __RAWPP_HPP__

#ifndef HAVE_MAT_H

#include <cassert>
class mxArray {};
#define prtmsg printf
#define prtwrn printf
#define prterr(x) printf(x); assert(false);
#define evalstr
#define msize_t size_t

#else

#define prtmsg mexPrintf
#define prterr(x) mexErrMsgTxt(x);
#define prtwrn mexWarnMsgTxt
#define evalstr mexEvalString
#define msize_t mwSize

#endif

#include <string>
#include "VXFile.hpp"

void read (const std::string& fname, Matrix<raw>* buf);

#endif
