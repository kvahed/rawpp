function [data, sync] = xrawpp (filename) 
%%
%  Load Siemens syngo MR files of VB/VD generation
%  Compile with mex -O rawpp.cpp
% 
%  [data, sync] = xrawpp ('filename'); 
%
%  Kaveh Vahedipour - NYU Langone Medcal Center - Jul 2014

% Call GUI to get filetype if not specified by commandline
    if (nargin < 1) || (isempty(filename))
        filename = file_gui();
    end
    if (nargout == 1)
        data = rawpp (filename);
    elseif (nargout == 2)
        [data, sync] = rawpp (filename);
    end
end

function [path_to_file] = file_gui()
    info_txt = 'Select file ';
    [fname,pathname]=uigetfile({'*.dat'   , 'VB/VD Binary (*.dat)';}, info_txt);
    if (isequal(fname,0) || isequal(pathname,0))
        error([TAG 'File selection cancelled!']);
    else
        path_to_file=fullfile(pathname,fname);
    end
end

