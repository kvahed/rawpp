#include "rawpp.hpp"
#include "Algos.hpp"

int main (const int nargs, const char** argv) {
    Matrix<raw> meas, column;
    
    read ("meas_MID2407_tpi_sr1_RO12000_p04_FID29563.dat", &meas);
    std::cout << std::endl;
    std::cout << "meas = squeeze(meas);" << std::endl;
    meas = squeeze(meas);
    std::cout << "size(meas):       " << size(meas) << std::endl ;
    std::cout << "meas(1,1,1):      " << meas(0,0,0) << std::endl;
    std::cout << "meas(256,6,10,1): " << meas(255,5,9,0) << std::endl<< std::endl;
    std::cout << "squeeze(sum(meas,3));" << std::endl;
    meas = squeeze(sum(meas,3))/size(meas,3);
    std::cout << "size(meas):       " << size(meas) << std::endl;
    std::cout << "meas(1,1,1):      " << meas(0,0,0) << std::endl;
    std::cout << "meas(256,6,10):   " << meas(255,5,9) << std::endl;
    std::cout << "meas(:,1,1):      " << std::endl;
    column = meas(CR(),CR(),0);

    Vector<raw> v = meas.Container();
    std::cout << column << std::endl;
    
    
    return 0;
}
